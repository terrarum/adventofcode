const parttwo = require('./parttwo');

const input = [
    '1-3 a: abcde',
    '1-3 b: cdefg',
    '2-9 c: ccccccccc',
]

test('part two', () => {
    expect(parttwo(input)).toBe(1);
});

