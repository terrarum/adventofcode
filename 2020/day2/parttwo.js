module.exports = function (input) {
    let valid = 0;

    input.forEach((line) => {
        const components = line.split(' ');

        if (components.length === 1) {
            return;
        }

        const first = components[0].split('-')[0];
        const second = components[0].split('-')[1];
        const character = components[1][0];
        const password = components[2];

        if (password[first - 1] === character && password[second - 1] === character) {
            return;
        }

        if (password[first - 1] === character || password[second - 1] === character) {
            valid++;
        }
    });

    return valid;
}