const partone = require('./partone');

const input = [
    '1-3 a: abcde',
    '1-3 b: cdefg',
    '2-9 c: ccccccccc',
]

test('part one', () => {
    expect(partone(input)).toBe(2);
});

