module.exports = function (input) {
    let valid = 0;

    input.forEach((line) => {
        const components = line.split(' ');

        if (components.length === 1) {
            return;
        }

        const min = components[0].split('-')[0];
        const max = components[0].split('-')[1];
        const character = components[1][0];
        const password = components[2];

        const count = password.split(character).length - 1;
        if (count >= min && count <= max) {
            valid++;
        }
    });

    return valid;
}