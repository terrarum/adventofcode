function getPosition(str, spacer, bottom) {
    let pos = 0;
    for (let i = 0; i < str.length; i++) {
        pos += str[i] === bottom ? spacer : 0;
        spacer /= 2;
    }
    return pos;
}

module.exports = function (input) {
    const boardingPasses = [];

    input.forEach((pass) => {
        const row = getPosition(pass.slice(0, 7), 64, 'B');
        const column = getPosition(pass.slice(7, 10), 4, 'R');
        const seat = row * 8 + column;
        const boardingPass = {
            row,
            column,
            seat,
        };

        boardingPasses.push(boardingPass);
    });

    return boardingPasses;
}