const partone = require('./partone');

const input = [
    'BFFFBBFRRR',
    'FFFBBBFRRR',
    'BBFFBBFRLL',
];

test('part one', () => {
    expect(partone(input)).toBe(820);
});

