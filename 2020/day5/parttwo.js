const boardingpass = require('./boardingpass');

module.exports = function (input) {
    let boardingPasses = boardingpass(input);

    boardingPasses = boardingPasses.sort((a, b) => {
        return a.seat - b.seat;
    });

    for (let i = 0, l = boardingPasses.length; i < l; i++) {
        const currentPass = boardingPasses[i];
        const nextPass = boardingPasses[i + 1];

        if (currentPass.seat + 2 === nextPass.seat) {
            return currentPass.seat + 1;
        }
    }
}