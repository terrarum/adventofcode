const boardingpass = require('./boardingpass');

const input = [
    'BFFFBBFRRR',
    'FFFBBBFRRR',
    'BBFFBBFRLL',
];

test('part one', () => {
    expect(boardingpass(input)).toStrictEqual([
        {
            row: 70,
            column: 7,
            seat: 567,
        },
        {
            row: 14,
            column: 7,
            seat: 119,
        },
        {
            row: 102,
            column: 4,
            seat: 820,
        },
    ]);
});

