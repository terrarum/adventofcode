const boardingpass = require('./boardingpass');

module.exports = function (input) {
    let boardingPasses = boardingpass(input);
    boardingPasses = boardingPasses.sort((a, b) => {
        return b.seat - a.seat;
    });

    return boardingPasses[0].seat;
}