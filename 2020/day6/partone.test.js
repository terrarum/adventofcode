const partone = require('./partone');

const input = [
    'abc',
    'a\nb\nc',
    'ab\nac',
    'a\na\na\na',
    'b'
];

test('part one', () => {
    expect(partone(input)).toBe(11);
});

