module.exports = function (input) {
    let result = 0;

    input.forEach((group) => {
        group = group.replace(/\n/g, '');
        result += new Set(group).size;
    })

    return result;
}