module.exports = function (input) {
    let result = 0;

    input.forEach((group) => {
        const groupAnswers = group.split('\n');
        const groupSize = groupAnswers.length;
        const answers = {};
        groupAnswers.forEach((memberAnswer) => {
            [...memberAnswer].forEach((answer) => {
                if (answers[answer] === undefined) {
                    answers[answer] = 0;
                }
                answers[answer]++;
            })
        });
        let toAdd = 0;
        Object.keys(answers).forEach((key) => {
            if (answers[key] === groupSize) {
                result += 1;
                toAdd += 1;
            }
        });
    })

    return result;
}