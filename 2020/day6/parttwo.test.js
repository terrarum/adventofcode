const parttwo = require('./parttwo');

const input = [
    'abc',
    'a\nb\nc',
    'ab\nac',
    'a\na\na\na',
    'b',
    ''
];

test('part two', () => {
    expect(parttwo(input)).toBe(6);
});

