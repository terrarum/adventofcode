const fs = require('fs');
const util = require('util');
const partone = require('./partone');
const parttwo = require('./parttwo');

const readFile = util.promisify(fs.readFile);

async function load() {
    const buf = await readFile('./input.txt');
    let str = buf.toString('utf8');
    if (str.substr(-1) === '\n') {
        str = str.substr(0, str.length - 1);
    }
    return str.split('\n\n');
}

async function init() {
    const input = await load();
    console.log(`Part one: ${partone(input)}`);
    console.log(`Part two: ${parttwo(input)}`);
}

init();
