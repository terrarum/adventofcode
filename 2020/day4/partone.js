function validatePassport(passport) {
    let validKeyCount = 0;
    const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
    Object.keys(passport).forEach((key) => {
        if (requiredFields.includes(key)) {
            validKeyCount++;
        }
    })

    return validKeyCount === requiredFields.length;
}

module.exports = function (input) {
    let result = 0;

    const rawPassports = [];
    input.forEach((passport) => {
        rawPassports.push(passport.replace(/\n/g, ' '));
    })

    const passports = [];
    rawPassports.forEach((rawPassport) => {
        const passport = {};
        const segments = rawPassport.split(' ');
        segments.forEach((segment) => {
            const kv = segment.split(':');
            passport[kv[0]] = kv[1];
        });
        passports.push(passport);
    });

    passports.forEach((passport) => {
        if (validatePassport(passport)) {
            result++;
        }
    })

    return result;
}