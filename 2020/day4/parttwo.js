function validateRange(num, min, max) {
    return parseInt(num) >= min && parseInt(num) <= max;
}

function validateHeight(height) {
    const value = parseInt(height);
    const format = height.replace(/[0-9]/g, '');
    if (format === 'in') {
        return validateRange(value, 59, 76);
    }
    else {
        return validateRange(value, 150, 193);
    }
}

function validateHairColor(color) {
    if (color[0] !== '#') return false;
    const hex = color.substring(1);
    if (hex.length !== 6) return false;
    return /[a-f0-9]+/.test(hex);
}

function validateEyeColor(color) {
    return ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes(color);
}

function validatePassport(passport) {
    let validKeyCount = 0;
    const requiredFields = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'];
    Object.keys(passport).forEach((key) => {
        if (requiredFields.includes(key)) {
            validKeyCount++;
        }
    });
    if (validKeyCount !== requiredFields.length) return false;

    if (!validateRange(passport.byr, 1920, 2002)) return false;
    if (!validateRange(passport.iyr, 2010, 2020)) return false;
    if (!validateRange(passport.eyr, 2020, 2030)) return false;
    if (!validateHeight(passport.hgt)) return false;
    if (!validateHairColor(passport.hcl)) return false;
    if (!validateEyeColor(passport.ecl)) return false;
    return passport.pid.length === 9;
}

module.exports = function (input) {
    const rawPassports = [];
    input.forEach((passport) => {
        rawPassports.push(passport.replace(/\n/g, ' '));
    })

    const passports = [];
    rawPassports.forEach((rawPassport) => {
        const passport = {};
        const segments = rawPassport.split(' ');

        segments.forEach((segment) => {
            const kv = segment.split(':');
            passport[kv[0]] = kv[1];
        });

        if (validatePassport(passport)) {
            passports.push(passport);
        }
    });

    return passports.length;
}