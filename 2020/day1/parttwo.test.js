const parttwo = require('./parttwo');

const input = [
    '1721',
    '979',
    '366',
    '299',
    '675',
    '1456'
]

test('part two', () => {
    expect(parttwo(input)).toBe(241861950);
});

