const partone = require('./partone');

const input = [
    '1721',
    '979',
    '366',
    '299',
    '675',
    '1456'
]

test('part one', () => {
    expect(partone(input)).toBe(514579);
});

