module.exports = function(input) {
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < input.length; j++) {
            for (let k = 0; k < input.length; k++) {
                const one = parseInt(input[i]);
                const two = parseInt(input[j]);
                const three = parseInt(input[k]);
                const sum = one + two + three;
                if (sum === 2020) {
                    return one * two * three;
                }
            }
        }
    }
}