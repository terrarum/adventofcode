module.exports = function(input) {
    for (let i = 0; i < input.length; i++) {
        for (let j = 0; j < input.length; j++) {
            const left = parseInt(input[i]);
            const right = parseInt(input[j]);
            const sum = left + right;
            if (sum === 2020) {
                return left * right;
            }
        }
    }
}