module.exports = function (input) {
    let result = 1;

    const slopes = [
        { x: 1, y: 1 },
        { x: 3, y: 1 },
        { x: 5, y: 1 },
        { x: 7, y: 1 },
        { x: 1, y: 2 },
    ];

    const width = input[0].length;

    slopes.forEach((slope) => {
        let slopeTreeCount = 0;
        let xPos = 0;

        for (let yPos = 0; yPos < input.length; yPos += slope.y) {
            const row = input[yPos];
            if (row[xPos % width] === '#') {
                slopeTreeCount++;
            }

            xPos += slope.x;
        }

        result *= slopeTreeCount;
    })

    return result;
}