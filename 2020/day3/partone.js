module.exports = function (input) {
    let result = 0;

    const slope = { x: 3,y: 1 };

    const width = input[0].length;
    let xPos = 0;

    for (let yPos = 0; yPos < input.length; yPos += slope.y) {
        const row = input[yPos];
        if (row[xPos % width] === '#') {
            result++;
        }
        xPos += slope.x;
    }

    return result;
}