function buildLines(input) {
    const lines = [];
    input.forEach(line => {
        if (!line) return;
        const coords = line.split(' -> ');
        const from = coords[0].split(',');
        const to = coords[1].split(',');
        lines.push({
            x1: parseInt(from[0]),
            y1: parseInt(from[1]),
            x2: parseInt(to[0]),
            y2: parseInt(to[1]),
        });
    });
    return lines;
}

function generateLine(low, high, other, isHorizontal) {
    const points = [];
    while (low <= high) {
        const line = isHorizontal ? {x: other, y: low} : {x: low, y: other};
        points.push(line);
        low++;
    }
    return points;
}

function generateDiagonalLine(line) {
    const points = [];
    
    const from = {x: line.x1, y: line.y1};
    const to = {x: line.x2, y: line.y2};

    let xDelta = from.x < to.x ? 1 : -1;
    let yDelta = from.y < to.y ? 1 : -1;

    while (from.x !== to.x + xDelta && from.y !== to.y + yDelta) {
        points.push({x: from.x, y: from.y});
        from.x += xDelta;
        from.y += yDelta;
    }

    return points;
}

function generatePoints(lines) {
    let points = [];
    lines.forEach(line => {
        if (line.x1 === line.x2) {
            // horizontal
            const low = line.y1 < line.y2 ? line.y1 : line.y2;
            const high = line.y1 > line.y2 ? line.y1 : line.y2;
            points = points.concat(generateLine(low, high, line.x1, true))
        } else if (line.y1 === line.y2) {
            // vertical
            const low = line.x1 < line.x2 ? line.x1 : line.x2;
            const high = line.x1 > line.x2 ? line.x1 : line.x2;
            points = points.concat(generateLine(low, high, line.y1, false))
        } else {
            points = points.concat(generateDiagonalLine(line));
        }
    });
    return points;
}

function generateMap(points) {
    const map = {};
    points.forEach(point => {
        const label = `${point.x}-${point.y}`;
        map[label] = map[label] ? map[label] + 1 : 1 ;
    });
    return map;
}

module.exports = function(input) {
    const lines = buildLines(input);
    const points = generatePoints(lines);
    const map = generateMap(points);

    let result = 0;
    for (const key in map) {
        if (map[key] > 1) {
            result++;
        }
    }
    return result;
}
