function prepData(input) {
    const data = {
        numbers: input[0].split(','),
        boards: [],
    };

    let board = {
        winner: false,
        cells: [],
    };
    for (let i = 2; i < input.length; i++) {
        const row = input[i];
        if (row === '') {
            data.boards.push(board);
            board = {
                winner: false,
                cells: [],
            };
        }
        else {
            board.cells = board.cells.concat(
                row.split(' ')
                    .filter(t => t !== '')
                    .map(t => ({ number: t, called: false }))
            );
        }
    }

    return data;
}

function updateBoards(boards, number) {
    boards.forEach(board => {
        board.cells.forEach(cell => {
            if (cell.number === number) {
                cell.called = true;
            }
        })
    });
}

function checkBoards(boards) {
    boards.forEach(board => {
        const rows = {'0': 0, '1': 0, '2': 0, '3': 0, '4': 0}
        const cols = {'0': 0, '1': 0, '2': 0, '3': 0, '4': 0}
        let boardSize = 5;
        for (let i = 0; i < board.cells.length; i++) {
            let row = Math.floor(i / boardSize);
            let col = i % boardSize;
            const cell = board.cells[i];
            if (cell.called) {
                rows[`${row}`] += 1;
                cols[`${col}`] += 1;

                if (rows[`${row}`] === boardSize || cols[`${col}`] === boardSize) {
                    board.winner = true;
                    return;
                }
            }
        }
    })
}

function getResult(board, number) {
    let unselectedSum = 0;
    board.cells.forEach(cell => {
        if (cell.called === false) {
            unselectedSum += parseInt(cell.number);
        }
    });

    return unselectedSum * number;
}

module.exports = function(input) {
    const data = prepData(input);
    let winner = null;
    let number = null;
    for(let i = 0; i < data.numbers.length; i++) {
        number = data.numbers[i];
        checkBoards(data.boards)
        updateBoards(data.boards, number)
        let losingBoards = 0;
        data.boards.forEach(board => {
            if (!board.winner) {
                losingBoards++;
                winner = board;
            }
        })
        if (losingBoards === 1) {
            break;
        }
    }
    return getResult(winner, number);
}
