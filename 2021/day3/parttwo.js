function reduce(input, shouldReturnMostCommon, prevIndex) {
    let index = prevIndex === undefined ? 0 : prevIndex;

    const counters = {
        '0': [],
        '1': [],
    }

    input.forEach((row) => {
        if (row === '') return;
        counters[row[index]].push(row);
    });

    let workingSet = null;
    if (shouldReturnMostCommon) {
        workingSet = counters['1'].length >= counters['0'].length ? counters['1'] : counters['0'];
    }
    else {
        workingSet = counters['1'].length < counters['0'].length ? counters['1'] : counters['0'];
    }

    if (workingSet.length === 1) {
        return parseInt(workingSet[0], 2);
    }
    else {
        return reduce(workingSet, shouldReturnMostCommon, index += 1);
    }
}


module.exports = function(input) {
    const oxygenRating = reduce(input, true);
    const co2Rating = reduce(input, false);

    return oxygenRating * co2Rating;
}
