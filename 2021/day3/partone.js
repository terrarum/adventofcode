function createCounters(count) {
    const counters = {};
    for (let i = 0; i < count; i++) {
        counters[`${i}`] = {
            '0': 0,
            '1': 0,
        }
    }

    return counters;
}

module.exports = function(input) {
    const counters = createCounters(input[0].length);

    for (let i = 0; i < input.length; i++) {
        const row = input[i];

        for (let rowIndex = 0; rowIndex < row.length; rowIndex++) {
            const char = row[rowIndex];
            counters[rowIndex][char]++;
        }
    }

    let gammaStr = '';
    let epsilonStr = '';
    for (let key in counters) {
        const counter = counters[key];
        gammaStr += counter['0'] > counter['1'] ? '0' : '1';
        epsilonStr += counter['0'] > counter['1'] ? '1' : '0';
    }

    return parseInt(gammaStr, 2) * parseInt(epsilonStr, 2);
}
