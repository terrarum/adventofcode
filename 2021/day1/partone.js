module.exports = function(input) {
  let count = 0;

  for (let i = 1; i < input.length; i++) {
    count += input[i] > input[i - 1] ? 1 : 0;
  }

  return count;
}
