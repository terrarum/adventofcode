module.exports = function(input) {
  let count = 0;
  let prev = Infinity;
  for (let i = 2; i < input.length; i++) {
    const m1 = parseInt(input[i]);
    const m2 = parseInt(input[i - 1]);
    const m3 = parseInt(input[i - 2]);

    const total = m1 + m2 + m3;

    if (total > prev) {
      count += 1;
    }

    prev = total;
  }

  return count;
}
