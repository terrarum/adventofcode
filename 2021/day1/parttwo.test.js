const parttwo = require('./parttwo');

const input = [
  '199',
  '200',
  '208',
  '210',
  '200',
  '207',
  '240',
  '269',
  '260',
  '263',
  '',
]

test('part two', async () => {
  expect(parttwo(input)).toBe(5);
});

