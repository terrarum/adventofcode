const partone = require('./partone');

const input = [
  '199',
  '200',
  '208',
  '210',
  '200',
  '207',
  '240',
  '269',
  '260',
  '263',
  '',
]

test('part one', async () => {
  expect(partone(input)).toBe(7);
});

