const partone = require('./partone');

const input = [
  'forward 5',
  'down 5',
  'forward 8',
  'up 3',
  'down 8',
  'forward 2',
  '',
]

test('part one', async () => {
  expect(partone(input)).toBe(150);
});

