const parttwo = require('./parttwo');

const input = [
  'forward 5',
  'down 5',
  'forward 8',
  'up 3',
  'down 8',
  'forward 2',
  '',
]

test('part two', async () => {
  expect(parttwo(input)).toBe(900);
});

