module.exports = function(input) {
  let distance = 0;
  let depth = 0;

  for (let i = 0; i < input.length; i++) {
    const instruction = input[i];
    const parts = instruction.split(' ');
    const direction = parts[0];
    const step = parseInt(parts[1]);
    if (direction === 'up') {
      depth -= step;
    }
    if (direction === 'down') {
      depth += step;
    }
    if (direction === 'forward') {
      distance += step;
    }
  }

  return distance * depth;
}
